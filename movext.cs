using System;
using System.IO;

public class Program
{
    public static void Main(string[] args)
    {
        string ext;

        try
        {
            ext = args[0];

            if(ext[0] != ".")
            {
                ext = "." + ext;
            }
        }
        catch(IndexOutOfRangeException e)
        {
            Console.Write("No extension passed.");
            Console.WriteLine("Terminating movext.");

            return;
        }

        string currentPath = Directory.GetCurrentDirectory();
        string stashPath = Path.Combine(currentPath, ext+"stash");

        Directory.CreateDirectory(stashPath);

        string[] files = Directory.GetFiles(currentPath);

        foreach(string file in files)
        {
            if(file.EndsWith(ext))
            {
                string src = Path.GetFileName(file);
                string dest = Path.Combine(stashPath, src);

                File.Copy(src, dest);
                File.Delete(src);
            }
        }
    }
}
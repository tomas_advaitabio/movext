# movext
movext is a small command line tool that moves all files with a specified extension into an organizational folder within the working directory
#
the extension by which files will be stashed must be passed to the program as its first and only command-line argument.
#
once run, movext will create a folder with the naming convention "{extension}stash" within the working directory and then move all files with the specified extension into that folder
#
it should be noted that if a file with the same name as the one being moved exists in the stash folder, the file being moved will overwrite it
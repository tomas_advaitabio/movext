import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MovExt
{
    public static void main(String[] args)
    {
        String ext;
        
        try
        {
            ext = args[0];

            if(!ext.startsWith("."))
            {
                ext = "." + ext;
            }
        }
        catch(IndexOutOfBoundsException e)
        {
            System.out.println("No extension passed.");
            System.out.println("Terminating movext.");

            return;
        }

        Path currentDir = Paths.get(".");
        
        List<Path> files = new ArrayList<Path>();

        try
        {
            System.out.println("Looking for files.");
            
            for (Path file : Files.list(currentDir).collect(Collectors.toList()))
            {
                if(!Files.isDirectory(file) && file.toString().endsWith(ext))
                {
                    files.add(file);
                }
            }

            if(files.isEmpty())
            {
                System.out.println("No files found.");
                System.out.println("Terminating movext.");

                return;
            }
        }
        catch(IOException e)
        {
            System.out.println(e);
            System.out.println("Failed to gather files.");
            System.out.println("Terminating movext.");

            return;
        }
        
        Path stashDir = Paths.get(ext+"stash");
        
        try
        {
            System.out.println("Creating stash directory.");

            Files.createDirectory(stashDir);
        }
        catch(FileAlreadyExistsException e)
        {
            System.out.println("Existing stash directory found.");
        }
        catch(IOException e)
        {
            System.out.println(e);
            System.out.println("Failed to find or create stash directory.");
            System.out.println("Terminating movext.");

            return;
        }
        
        for(Path file : files)
        {
            System.out.println("Moving files.");

            try
            {
                Path destFile = new File(stashDir.toString(), file.toString()).toPath();
                Files.move(file, destFile, StandardCopyOption.REPLACE_EXISTING);
            }
            catch(IOException e)
            {
                System.out.println(e);
                System.out.println("Failed to move file.");
                System.out.println("Attempting to continue.");
                continue;
            }

            System.out.println("Done!");
        }
    }
}
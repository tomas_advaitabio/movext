import os

ext = ""

if len(os.sys.argv) > 1:
    ext = os.sys.argv[1]
else:
    print("No extension passed.")
    print("Terminating movext.")
    quit()

if not "." in ext or len(ext) < 2:
    print("Invalid extension.")
    print("Terminating movext.")
    quit()

searchDir = os.getcwd()
stashDir = os.path.join(searchDir, ext + "stash")

if not os.path.exists(stashDir):
    os.mkdir(stashDir)

for candidate in os.listdir(searchDir):
    if candidate.endswith(ext):
        os.rename(candidate, os.path.join(stashDir, candidate))
